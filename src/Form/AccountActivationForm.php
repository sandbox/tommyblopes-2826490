<?php

namespace Drupal\account_activation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Render\Element\Email;
use Drupal\user\UserStorageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form allowing user to request for an account activation link when blocked.
 */
class AccountActivationForm extends FormBase {
  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a AccountActivationForm object.
   *
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(UserStorageInterface $user_storage, LanguageManagerInterface $language_manager) {
    $this->userStorage = $user_storage;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('user'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'account_activation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Username or email address'),
      '#size' => 60,
      '#maxlength' => max(USERNAME_MAX_LENGTH, Email::EMAIL_MAX_LENGTH),
      '#required' => TRUE,
      '#attributes' => array(
        'autocorrect' => 'off',
        'autocapitalize' => 'off',
        'spellcheck' => 'false',
        'autofocus' => 'autofocus',
      ),
    );
    // Allow logged in users to request this also.
    $user = $this->currentUser();
    if ($user->isAuthenticated()) {
      $form['name']['#type'] = 'value';
      $form['name']['#value'] = $user->getEmail();
      $form['mail'] = array(
        '#prefix' => '<p>',
        '#markup' => $this->t('Account activation instructions will be mailed to %email. You must log out to use the account activation link in the email.', array('%email' => $user->getEmail())),
        '#suffix' => '</p>',
      );
    }
    else {
      $form['mail'] = array(
        '#prefix' => '<p>',
        '#markup' => $this->t('Account activation instructions will be sent to your registered email address.'),
        '#suffix' => '</p>',
      );
      $form['name']['#default_value'] = $this->getRequest()->query->get('name');
    }
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => $this->t('Submit'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $name = trim($form_state->getValue('name'));
    // Try to load by email.
    $users = $this->userStorage->loadByProperties(array('mail' => $name));
    if (empty($users)) {
      // No success, try to load by name.
      $users = $this->userStorage->loadByProperties(array('name' => $name));
    }
    $account = reset($users);
    if ($account && $account->id() && !$account->isActive()) {
      $form_state->setValueForElement(array('#parents' => array('account')), $account);
    }
    else {
      $form_state->setErrorByName('name', $this->t('%name is not recognized as a username or an email address, or the account is already active.', array('%name' => $name)));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $account = $form_state->getValue('account');

    $timestamp = time();
    $token = Crypt::randomBytesBase64(50);
    $_SESSION['account_activation_' . $account->id()] = hash('sha256', $timestamp . $token);

    // Mail one time account activation URL and instructions.
    $mail = account_activation_mail_notify($account, $timestamp, $token, $langcode);
    if ($mail) {
      $this->logger('user')->notice('Account activation instructions mailed to %name at %email.', array('%name' => $account->getUsername(), '%email' => $account->getEmail()));
      drupal_set_message($this->t('Further instructions have been sent to your email address.'));
    }

    $form_state->setRedirect('user.page');
  }

}

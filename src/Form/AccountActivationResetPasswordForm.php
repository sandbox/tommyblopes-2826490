<?php

namespace Drupal\account_activation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserStorageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form allowing user to reset its password when accessing the secure URL.
 */
class AccountActivationResetPasswordForm extends FormBase {
  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a AccountActivationForm object.
   *
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(UserStorageInterface $user_storage, LanguageManagerInterface $language_manager) {
    $this->userStorage = $user_storage;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('user'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'account_activation_reset_password_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $route_match = \Drupal::service('current_route_match');
    $user = $route_match->getParameter('user');
    $timestamp = $route_match->getParameter('timestamp');
    $hash = $route_match->getParameter('hash');

    $form['pass'] = array(
      '#type' => 'password_confirm',
      '#size' => 25,
      '#description' => $this->t('To change the current user password, enter the new password in both fields.'),
      '#required' => TRUE,
    );

    $form['description'] = array(
      '#markup' => '<p>' . $this->t('You are using your one-time account activation link that is valid for 1 day only after you recieve it. Please change your password to activate your account.') . '</p>',
    );

    $form['user'] = array(
      '#type' => 'hidden',
      '#value' => $user,
    );

    $form['timestamp'] = array(
      '#type' => 'hidden',
      '#value' => $timestamp,
    );

    $form['hash'] = array(
      '#type' => 'hidden',
      '#value' => $hash,
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => $this->t('Submit'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('account_activation.settings');
    $user = $form_state->getValue('user');
    $timestamp = $form_state->getValue('timestamp');
    $hash = $form_state->getValue('hash');
    $account = $this->userStorage->load($user);
    // Configured hours converted in seconds.
    $timeout = $config->get('expiration_time') * 3600;
    if (!empty($account) && !$account->isActive() && !empty($timestamp) && !empty($hash) &&
        isset($_SESSION['account_activation_' . $account->id()]) &&
        $_SESSION['account_activation_' . $account->id()] == hash('sha256', $timestamp . $hash) &&
        time() - $timestamp <= $timeout) {
      unset($_SESSION['account_activation_' . $account->id()]);
      $account->setPassword($form_state->getValue('pass'));
      $account->activate();
      $account->save();
      drupal_set_message($this->t('Your account has been successfully activated and you can now log in using your updated password.'));
      $form_state->setRedirect('user.login');
    }
    else {
      drupal_set_message($this->t('You have tried to use a one-time account activation link that is invalid, has expired or has already been used. It is also possible that the account you are trying to activate is already active. Please request a new one using the form below or try to log in your account.'), 'error');
      $form_state->setRedirect('account_activation.form');
    }
  }

}

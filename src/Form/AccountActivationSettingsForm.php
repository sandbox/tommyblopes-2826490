<?php

namespace Drupal\account_activation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form allowing admin to configure Account Activation module.
 */
class AccountActivationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'account_activation_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'account_activation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('account_activation.settings');

    $form['expiration_time'] = array(
      '#type' => 'number',
      '#title' => $this->t("Account activation secure URL expiration time, in hours. Number of hours after which the secure URL won't work anymore."),
      '#default_value' => $config->get('expiration_time'),
      '#min' => 1,
      '#field_suffix' => $this->t('hours'),
      '#required' => TRUE,
      '#size' => 8,
    );

    $form['mail_subject'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Email subject that will be sent to the user when an account activation is requested.'),
      '#default_value' => $config->get('mail_subject'),
      '#required' => TRUE,
    );

    $form['mail_template'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Email template that will be sent to the user when an account activation is requested.'),
      '#default_value' => $config->get('mail_template'),
      '#required' => TRUE,
      '#rows' => 12,
    );

    $markup = '<p>' . $this->t('The strings could be personalized using the following placeholders:') . '</p>';
    $markup .= '<ul><li>' . $this->t('%date% : The (formatted) date and time of the operation') . '</li>';
    $markup .= '<li>' . $this->t('%ip% : The IP Address performing the operation') . '</li>';
    $markup .= '<li>' . $this->t('%username% : The username of the user requesting the operation') . '</li>';
    $markup .= '<li>' . $this->t('%email% : If the user exists, its email') . '</li>';
    $markup .= '<li>' . $this->t('%uid% :  If the user exists, its uid') . '</li>';
    $markup .= '<li>' . $this->t("%site% : The configured site's name") . '</li>';
    $markup .= '<li>' . $this->t('%uri% : The base url of the Drupal site') . '</li>';
    $markup .= '<li>' . $this->t('%activation_uri% : Direct link to account activation form path') . '</li>';
    $markup .= '<li>' . $this->t('%expiration_time% : Configured secure url expiration time in hours') . '</li>';
    $markup .= '<li>' . $this->t('%secure_link% : The secure URL that the user can access to activate its account') . '</li></ul>';
    $form['mail_template_help'] = array(
      '#markup' => $markup,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('account_activation.settings');
    $config->set('expiration_time', $form_state->getValue('expiration_time'))->save();
    $config->set('mail_subject', $form_state->getValue('mail_subject'))->save();
    $config->set('mail_template', $form_state->getValue('mail_template'))->save();
    parent::submitForm($form, $form_state);
  }

}

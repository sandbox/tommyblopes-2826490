
Account Activation
--------------
This module was developed to give users a way to activate their account through
dedicated URLs and a secure process. By default, Drupal has no way to allow a user
to unlock its own account after it has been blocked for different reasons such as
a forgotten password leading to too many failed login attempts, or else.

With Account Activation module, blocked users are provided with the URL /user/activation
presenting a form where they can specify their username/email. By submitting
- if the account exists and is blocked -, the user will recieve an email containing
a secure one-time token-based URL and the instructions to unlock their account.
Accessing this secure URL will provide the user with a form to reset their password,
and activate/unlock their account on success submit.

A Notice event log is saved with the "user" type everytime an account activation
notification has been requested and sent.


Installation
------------
To install copy the account_activation folder in your modules directory. Go to
the Admin Modules page at /admin/modules and under the "Other" frame the
"Account Activation" module Item will appear in the list. Enable the Checkbox
and save the configuration.


Configuration options
---------------------
You should have the 'administer site configuration' permission to configure the
Account Activation module.

Go to the Account Activation Settings page at /admin/config/people/account-activation
and a form with the module options will be shown.

 - Account activation secure URL expiration time:
   The secure URL expiration time, in hours. Number of hours after which the secure
   URL won't work anymore and the user will have to request a new one.
   Default to 24 hours.

 - Notification mail subject:
   The email subject that will be sent to the user when an account activation is requested.
   Default to:
   "Account activation request for %username% at %site%"

 - Notification mail template:
   The email template that will be sent to the user when an account activation is requested.

   The strings could be personalized using the following placeholders:

    %date%                  :  The (formatted) date and time of the operation
    %ip%                    :  The IP Address performing the operation
    %username%              :  The username of the user requesting the operation
    %email%                 :  If the user exists, its email
    %uid%                   :  If the user exists, its uid
    %site%                  :  The configured site's name
    %uri%                   :  The base url of the Drupal site
    %activation_uri%        :  Direct link to account activation form path
    %expiration_time%       :  Configured secure url expiration time in hours
    %secure_link%           :  The secure URL that the user can access to activate its account

   Default to:

   "%username%,
    
    A request to activate your account has been made at %site%.

    You can now attempt to activate your account by clicking this link or copying
    and pasting it into your browser:

    %secure_link%

    This link can only be used once to activate the account and will lead you to
    a page where you can set a new password. It expires after %expiration_time%
    hours and nothing will happen if it's not used.

    -- %site% team
